import { CapacitorConfig } from '@capacitor/cli';

const config: CapacitorConfig = {
  appId: 'io.ionic.starter',
  appName: 'ExamenParcial3_SantiagoNunez',
  webDir: 'www',
  server: {
    androidScheme: 'https'
  }
};

export default config;
