import { Component } from '@angular/core';
import { ClientesService } from '../services/clientes.service';
import { MascotasClienteService } from '../services/mascotas-cliente.service';
import {VacunasMascotaService } from '../services/vacunas-mascota.service';

import {ClientesResponse} from '../../app/interfaces/clientes'
import {MascotasPorClienteResponse} from '../../app/interfaces/mascotasPorCliente'
import {VacunasPorMascotaResponse} from '../../app/interfaces/vacunasPorMascota'

@Component({
  selector: 'app-home',
  templateUrl: 'home.page.html',
  styleUrls: ['home.page.scss'],
})
export class HomePage {
  seleccionMascotaID!: string;
  clientes: ClientesResponse[] = [];
  mascotas: MascotasPorClienteResponse[] = [];
  vacunas: VacunasPorMascotaResponse[] = [];

  constructor(
    private serviceClientes: ClientesService,
    private serviceMacostasCliente: MascotasClienteService,
    private serviceVacunasMascota: VacunasMascotaService
  ) {}

  onClienteChange(event: any) {
    const ClienteID: string = event.target.value;
    this.serviceMacostasCliente.getMascotas(ClienteID).subscribe((resp) => {
      this.mascotas = <MascotasPorClienteResponse[]>resp;
    });
  }

  onMascotaChange(event: any) {
    this.seleccionMascotaID = event.target.value;
  }

  onClick() {
    if (this.seleccionMascotaID) {
      this.serviceVacunasMascota.getVacunas(this.seleccionMascotaID).subscribe((resp) => {
        this.vacunas = <VacunasPorMascotaResponse[]>resp;
        console.log(this.vacunas)
      });
    }
  }

  ngOnInit() {
    this.serviceClientes.getClientes().subscribe((resp) => {
      this.clientes = <ClientesResponse[]>resp;
    });
  }
}
