import { Injectable } from '@angular/core';
import {HttpClient} from '@angular/common/http'

@Injectable({
  providedIn: 'root'
})
export class VacunasMascotaService {

  constructor(private http:HttpClient) { }

  getVacunas(MascotaID: string){
    const url = `https://www.hostcatedral.com/api/appCatalogoLibro/public/getVacunasPorMascota/${MascotaID}`;
    return this.http.get(url)
  }
}
