import { TestBed } from '@angular/core/testing';

import { VacunasMascotaService } from './vacunas-mascota.service';

describe('VacunasPorMascotaService', () => {
  let service: VacunasMascotaService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(VacunasMascotaService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
