import { TestBed } from '@angular/core/testing';

import { MascotasClienteService } from './mascotas-cliente.service';

describe('MascotasPorClienteService', () => {
  let service: MascotasClienteService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(MascotasClienteService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
